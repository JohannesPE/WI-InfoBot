import json 
import requests
import time
import urllib
from requests import get
from bs4 import BeautifulSoup
import logging
import os


logging.basicConfig(filename='/home/pi//WIInfoBot/WIInfoBot.log',level=logging.DEBUG)
logging.debug('This message should go to the log file')

logging.warning('And this, too')

TOKEN ="614036660:AAEEbjWjRE1w8ohXZ09P8ggYNzrJHxthIx4"
URL = "https://api.telegram.org/bot{}/".format(TOKEN)
HELP = "Der Befehl ist nicht vorhanden.\nFolgende Befehle sind verfuegbar: \nan  - fuegt dich zu der Empfaengerliste hinzu \nalle - Zeigt alle verfuegbaren Klausuren an\taus - loescht dich wieder aus der Liste"

#url = 'https://hs-flensburg.de/node/3784'
url = 'https://hs-flensburg.de/node/3965'


def getInfo():
    response = get(url)
    
    file = open('klausuren.txt','r')
    parts = []
    for line in file:
        parts.append(line[:-1])
    
    file.close()

    print(parts)
    soup = BeautifulSoup(response.text, 'html.parser')
    elem=soup.find_all("div","field--label-above")
    list = elem[0].find_all('a')

    file = open('klausuren.txt','a')
    data=[]
    for i in list:
        text=i.text.encode('utf-8')
        if text not in parts:
            data.append(i)
            file.write(text + os.linesep);
    file.close()
 
    return(data)


def getAll():
    response = get(url)
    
    soup = BeautifulSoup(response.text, 'html.parser')
    elem=soup.find_all("div","field--label-above")
    list = elem[0].find_all('a')
    return(list)

def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content

def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js

def get_updates(offset=None):
    url = URL + "getUpdates"
    if offset:
        url += "?offset={}".format(offset)
    js = get_json_from_url(url)
    return js

def get_last_update_id(updates):
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)

def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)

def echo_all(updates):
    for update in updates["result"]:
        try:
            text = update["message"]["text"]
            chat = update["message"]["chat"]["id"]
            send_message(text, chat)
        except Exception as e:
            print(e)

def getChatIds():
    chats=[];
    file = open('chats.txt','r')
    for line in file:
        chats.append(line[:-1])
    file.close()
    return chats;

def send_message(text, chat_id):
    split= text.split(" ")
    cmd=split[0].lower()

    if (cmd in  'an'):
        if str(chat_id) in getChatIds():
            message="Du bist bereits eingetragen"    
        else:
            file = open('chats.txt','a')
            file.write(str(chat_id) + os.linesep)
            file.close()
            message="Du wurdest hinzugefuegt"
    elif(cmd in 'alle'):    
        alle= getAll()
        message=""
        for elem in alle:
            message= message+createMessage(elem)

    elif(cmd in 'aus'):
        chats= getChatIds();
        file = open('chats.txt','w') 
        chats.remove(str(chat_id))            
        for line in chats:
            file.write(str(line) + os.linesep)
        file.close()    
        message="Du wurdest erfolgreich entfernt"
        
    else:
        message=HELP

    url = URL + "sendMessage?&parse_mode=HTML&text={}&chat_id={}".format(message, chat_id)
    get_url(url)


def createMessage(elem):
    name=elem.text.encode('utf-8')
    link='<a href="'+elem['href']+'"> LINK</a>'
    message= name[:-4] + " ist online!" + link +"\n"
    return message

def main():
    last_update_id = None
    while True:
        updates = get_updates(last_update_id)
        if len(updates["result"]) > 0:
	    #print("\n UPDATES")
            last_update_id = get_last_update_id(updates) + 1
            echo_all(updates)
            
        result= getInfo()  
        for elem in result:
            message=createMessage(elem)
            file = open('chats.txt','r')
            for chat_id in file:
                #print(line)
                url = URL + "sendMessage?&parse_mode=HTML&text={}&chat_id={}".format(message, chat_id)
                get_url(url)
            file.close()
            
            
        time.sleep(5)

if __name__ == '__main__':
    main()
   





