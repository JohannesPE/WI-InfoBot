MYPROG="WIInfoBot.py"
RESTART="python /home/pi/WIInfoBot/WIInfoBot.py"
# find myprog pid
ps aux | grep  ${MYPROG} | grep python
# if not running
if [ $? -ne 0 ]
then
   $RESTART
fi
